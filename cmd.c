/**
 * Operating Systems 2013-2017 - Assignment 2
 *
 * Daniela Ene, 332CC
 *
 */

#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>

#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "cmd.h"
#include "utils.h"


#define READ		0
#define WRITE		1
#define MAX_LINE_SIZE		256
#define MAX_ARGS		8

#define ERROR			0
#define SIMPLE			1
#define REDIRECT		2
#define PIPE			3
#define SET_VAR			4
#define EXIT_CMD		5
#define INPUT			6

/**
 * Internal change-directory command.
 */
static bool shell_cd(simple_command_t *s)
{
	int dup_out;
	int fd;
	int rc;
	int ret;

	if (s->out != NULL) {
		const char *out_file = s->out->string;

		fflush(stdout);
		/* backup STDOUT */
		dup_out = dup(STDOUT_FILENO);

		/* open output file */
		fd = open(out_file, O_WRONLY | O_CREAT | O_TRUNC, 0644);

		/* redirect STDOUT to output file */
		rc = dup2(fd, STDOUT_FILENO);
		if (rc == -1) {
			perror("dup2");
			exit(EXIT_FAILURE);
		}

		/* call chdir command */
		ret = chdir(s->params->string);

		/*close output file*/
		close(fd);
		fflush(stdout);

		/*restore stdout*/
		rc = dup2(dup_out, STDOUT_FILENO);
		if (rc == -1) {
			perror("fork");
			exit(EXIT_FAILURE);
		}

		/*close backup descriptor*/
		close(dup_out);

	} else {
		/* call chdir command */
		ret = chdir(s->params->string);
		fflush(stdout);
	}

	return ret;
}

/**
 * Internal exit/quit command.
 */
static int shell_exit(void)
{
	/* execute exit/quit */
	return SHELL_EXIT;

}

static void do_redirect(int filedes, const char *filename, int io_flags)
{
	int ret;
	int fd;

	if (io_flags == INPUT) {

		/*open file with RONLY permission*/
		fd = open(filename, O_RDONLY);

		if (fd < 0) {
			perror("open");
			exit(EXIT_FAILURE);
		}

		/* redirect filedes into file */
		ret = dup2(fd, filedes);

		if (ret < 0) {
			perror("dup2");
			exit(EXIT_FAILURE);
		}

		/* close file */
		close(fd);
	}
	if (io_flags == IO_REGULAR) {

		/* open file with WRONLY permission */
		fd = open(filename, O_WRONLY | O_CREAT | O_TRUNC, 0644);

		if (fd < 0) {
			perror("open");
			exit(EXIT_FAILURE);
		}

		/* redirect filedes into file */
		ret = dup2(fd, filedes);

		if (ret < 0) {
			perror("dup2");
			exit(EXIT_FAILURE);
		}

		/* close file */
		close(fd);
	}
	if (io_flags == IO_OUT_APPEND || io_flags == IO_ERR_APPEND) {

		/* open file with APPEND permission */
		fd = open(filename, O_RDWR | O_CREAT | O_APPEND, 0644);

		if (fd < 0) {
			perror("open");
			exit(EXIT_FAILURE);
		}

		/* redirect filedes into file */
		ret = dup2(fd, filedes);

		if (ret < 0) {
			perror("dup2");
			exit(EXIT_FAILURE);
		}

		/* close file */
		close(fd);
	}
}


static void free_resources(char *verb, char *out_file, char *err_file,
	char *in_file, char **param_list, int size)
{
	int i;

	free(out_file);
	free(err_file);
	free(in_file);
	free(verb);
	for (i = 0; i < size; i++)
		free(param_list[i]);
	free(param_list);
}
/**
 * Parse a simple command (internal, environment variable assignment,
 * external command).
 */
static int parse_simple(simple_command_t *s, int level, command_t *father)
{

	pid_t pid, wait_ret;
	int status;
	int size = 0;
	char *out_file = get_word(s->out);
	char *err_file = get_word(s->err);
	char *in_file = get_word(s->in);
	char *verb = get_word(s->verb);
	char **param_list = get_argv(s, &size);

	/* sanity checks */
	if (s == NULL)
		exit(EXIT_FAILURE);

	/* if command is exit, execute the command */
	if (strcmp(s->verb->string, "exit") == 0 ||
		strcmp(s->verb->string, "quit") == 0) {

		free_resources(verb, out_file, err_file,
			in_file, param_list, size);
		return shell_exit();
	}

	/* if command is cd, execute the command */
	if (strcmp(s->verb->string, "cd") == 0 && s->params != NULL) {

		free_resources(verb, out_file, err_file,
			in_file, param_list, size);
		return shell_cd(s);
	}

	/*fork new process*/
	pid = fork();

	switch (pid) {
	case -1:
		perror("fork");
		exit(EXIT_FAILURE);

	case 0:
		/*if the operation is &>*/
		if (s->out != NULL && s->err != NULL &&
			strcmp(out_file, err_file) == 0) {

			do_redirect(STDERR_FILENO, err_file,
				IO_REGULAR);
			do_redirect(STDOUT_FILENO, out_file,
				IO_OUT_APPEND);

		} else {
			/*if the operation is >*/
			if (s->out != NULL)
				do_redirect(STDOUT_FILENO,
					out_file, s->io_flags);
			/*if the operation is 2>*/
			if (s->err != NULL)
				do_redirect(STDERR_FILENO,
					err_file, s->io_flags);
			/*if the operation is <*/
			if (s->in != NULL)
				do_redirect(STDIN_FILENO,
					in_file, INPUT);

		}
		/*load executable in child*/
		execvp(verb, param_list);

		fprintf(stdout, "Execution failed for '%s'\n", s->verb->string);
		fflush(stdout);

		exit(EXIT_FAILURE);

	default:
		/*wait for child*/
		wait_ret = waitpid(pid, &status, 0);
		if (wait_ret == -1) {
			perror("waitpid");
			exit(EXIT_FAILURE);
		}
	}

	free_resources(verb, out_file, err_file,
		in_file, param_list, size);
	/*return exit status*/
	return status;
}

/**
 * Process two commands in parallel, by creating two children.
 */
static bool do_in_parallel(command_t *cmd1, command_t *cmd2, int level,
		command_t *father)
{
	pid_t pid1, pid2;
	int wait_ret1, wait_ret2;
	int status1 = EXIT_CMD, status2 = EXIT_CMD;
	int ret = EXIT_CMD;

	/*create first child*/
	pid1 = fork();
	if (pid1 == -1) {
		perror("fork");
		exit(EXIT_FAILURE);
	} else {
		if (pid1 == 0) {
			/*first child executes first command*/
			ret = parse_command(cmd1, level, father);
			exit(EXIT_FAILURE);
		}
		/*create second child*/
		pid2 = fork();
		if (pid2 == -1) {
			perror("fork");
			exit(EXIT_FAILURE);
		} else {

			if (pid2 == 0) {
				/*second child executes second command*/
				ret = parse_command(cmd2, level, father);
				exit(EXIT_FAILURE);
			} else {
				/*wait for second child*/
				wait_ret2 = waitpid(pid2, &status2, 0);
				if (wait_ret2 == -1) {
					perror("waitpid");
					exit(EXIT_FAILURE);
				}
			}
		}
		/*wait for first child*/
		wait_ret1 = waitpid(pid1, &status1, 0);
		if (wait_ret1 == -1) {
			perror("waitpid");
			exit(EXIT_FAILURE);
		}
	}
	/*return exit status*/
	return ret;
}

/**
 * Run commands by creating an anonymous pipe (cmd1 | cmd2)
 */
static bool do_on_pipe(command_t *cmd1, command_t *cmd2, int level,
		command_t *father)
{
	int status, status1;
	pid_t pd[2];
	int ret;
	pid_t wait_ret;
	int pid;
	int dup_out, dup_in;

	/*create pipe*/
	pipe(pd);

	/*create child process*/
	pid = fork();

	switch (pid) {
	case -1:

		perror("fork");
		exit(EXIT_FAILURE);

	case 0:
		/*backup STDOUT*/
		dup_out = dup(STDOUT_FILENO);
		if (dup_out == -1) {
			perror("dup");
			exit(EXIT_FAILURE);
		}

		/*remap STDOUT of child process to write ending of pipe*/
		ret = dup2(pd[1], STDOUT_FILENO);
		if (ret == -1) {
			perror("dup2");
			exit(EXIT_FAILURE);
		}

		/*close both endings of pipe*/
		close(pd[0]);
		close(pd[1]);

		/*execute command*/
		status = parse_command(cmd1, level + 1, father);

		/*restore STDOUT*/
		ret = dup2(dup_out, STDOUT_FILENO);
		if (ret == -1) {
			perror("dup2");
			exit(EXIT_FAILURE);
		}

		/*close backup descriptor*/
		close(dup_out);
		exit(EXIT_FAILURE);

	default:
		/*backup STDIN*/
		dup_in = dup(STDIN_FILENO);
		if (dup_in == -1) {
			perror("dup");
			exit(EXIT_FAILURE);
		}

		/*remap STDIN of parent process to read ending of pipe*/
		ret = dup2(pd[0], STDIN_FILENO);
		if (ret == -1) {
			perror("dup2");
			exit(EXIT_FAILURE);
		}

		/*close both endings of pipe*/
		close(pd[0]);
		close(pd[1]);

		/*execute command*/
		status = parse_command(cmd2, level + 1, father);

		/*restore STDIN*/
		ret = dup2(dup_in, STDIN_FILENO);
		if (ret == -1) {
			perror("dup2");
			exit(EXIT_FAILURE);
		}

		/*close backup descriptor*/
		close(dup_in);

		/*wait for child*/
		wait_ret = waitpid(pid, &status1, 0);
		if (wait_ret == -1) {
			perror("waitpid");
			exit(EXIT_FAILURE);
		}
	}

	return status;
}

static int set_var(const char *var, const char *value)
{
	/* set the environment variable */
	return setenv(var, value, 1);
}

/**
 * Parse and execute a command.
 */
int parse_command(command_t *c, int level, command_t *father)
{

	int status;

	/* sanity checks */
	if (c == NULL)
		exit(EXIT_FAILURE);

	if (c->scmd != NULL && c->scmd->verb->next_part != NULL &&
		strcmp(c->scmd->verb->next_part->string, "=") == 0) {
		status = set_var(c->scmd->verb->string,
			c->scmd->verb->next_part->next_part->string);
		return status;
	}

	if (c->op == OP_NONE) {
		/* execute a simple command */
		status = parse_simple(c->scmd, level, father);
		return status;
	}

	switch (c->op) {
	case OP_SEQUENTIAL:
		/* execute the commands one after the other */
		status = parse_command(c->cmd1, level + 1, c);
		status = parse_command(c->cmd2, level + 1, c);
		break;

	case OP_PARALLEL:
		/* execute the commands simultaneously */
		status = do_in_parallel(c->cmd1, c->cmd2, level, father);
		break;

	case OP_CONDITIONAL_NZERO:
		/* execute the second command only if the first one
		 * returns non zero
		 */
		status = parse_command(c->cmd1, level + 1, c);
		if (status != 0)
			status = parse_command(c->cmd2, level + 1, c);
		break;

	case OP_CONDITIONAL_ZERO:
		/* execute the second command only if the first one
		 * returns zero
		 */
		status = parse_command(c->cmd1, level + 1, c);
		if (status == 0)
			status = parse_command(c->cmd2, level + 1, c);
		break;

	case OP_PIPE:
		/* redirect the output of the first command to the
		 * input of the second
		 */
		status = do_on_pipe(c->cmd1, c->cmd2, level, father);
		break;

	default:
		return SHELL_EXIT;
	}

	return status;
}
